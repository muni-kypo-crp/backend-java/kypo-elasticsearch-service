package cz.muni.ics.kypo.elasticsearch.data.indexpaths;

/**
 * The type Abstract kypo index path.
 */
public abstract class AbstractKypoIndexPath {

    /**
     * The constant KYPO_EVENTS_INDEX.
     */
    public static final String KYPO_EVENTS_INDEX = "kypo.events.trainings";
    public static final String KYPO_ADAPTIVE_EVENTS_INDEX = "kypo.events.adaptive.trainings";
    public static final String KYPO_CONSOLE_COMMANDS_INDEX = "kypo.logs.console";
}
