package cz.muni.ics.kypo.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KypoElasticsearchServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(KypoElasticsearchServiceApplication.class, args);
    }

}
