package cz.muni.ics.kypo.elasticsearch.data.indexpaths;

/**
 * The type Abstract kypo elastic term query fields.
 */
public abstract class AbstractKypoElasticTermQueryFields {

    /**
     * The constant KYPO_ELASTICSEARCH_TIMESTAMP.
     */
    public static final String KYPO_ELASTICSEARCH_TIMESTAMP = "timestamp";
    /**
     * The constant KYPO_ELASTICSEARCH_TIMESTAMP_MS.
     */
    public static final String KYPO_ELASTICSEARCH_TIMESTAMP_MS = "timestamp_ms";
    /**
     * The constant KYPO_ELASTICSEARCH_TIMESTAMP_MS.
     */
    public static final String KYPO_ELASTICSEARCH_TIMESTAMP_STR = "timestamp_str";
    /**
     * The constant KYPO_ELASTICSEARCH_TRAINING_DEFINITION_ID.
     */
    public static final String KYPO_ELASTICSEARCH_TRAINING_DEFINITION_ID = "training_definition_id";
    /**
     * The constant KYPO_ELASTICSEARCH_TRAINING_INSTANCE_ID.
     */
    public static final String KYPO_ELASTICSEARCH_TRAINING_INSTANCE_ID = "training_instance_id";
    /**
     * The constant KYPO_ELASTICSEARCH_TRAINING_RUN_ID.
     */
    public static final String KYPO_ELASTICSEARCH_TRAINING_RUN_ID = "training_run_id";
    /**
     * The constant KYPO_ELASTICSEARCH_SANDBOX_ID.
     */
    public static final String KYPO_ELASTICSEARCH_SANDBOX_ID = "sandbox_id";
    /**
     * The constant KYPO_ELASTICSEARCH_POOL_ID.
     */
    public static final String KYPO_ELASTICSEARCH_POOL_ID = "pool_id";
    /**
     * The constant KYPO_ELASTICSEARCH_LEVEL_ID.
     */
    public static final String KYPO_ELASTICSEARCH_LEVEL_ID = "level";
    /**
     * The constant KYPO_ELASTICSEARCH_PHASE_ID.
     */
    public static final String KYPO_ELASTICSEARCH_PHASE_ID = "phase_id";
    /**
     * The constant KYPO_ELASTICSEARCH_PHASE_ID.
     */
    public static final String KYPO_ELASTICSEARCH_TASK_ID = "task_id";
    /**
     * The constant KYPO_ELASTICSEARCH_GAME_TIME.
     */
    public static final String KYPO_ELASTICSEARCH_GAME_TIME = "game_time";
    /**
     * The constant KYPO_ELASTICSEARCH_EVENT_TYPE.
     */
    public static final String KYPO_ELASTICSEARCH_EVENT_TYPE = "type";
    /**
     * The constant KYPO_ELASTICSEARCH_COMMAND_TYPE.
     */
    public static final String KYPO_ELASTICSEARCH_COMMAND_TYPE = "command_type";
    /**
     * The constant KYPO_ELASTICSEARCH_ANSWER_CONTENT.
     */
    public static final String KYPO_ELASTICSEARCH_ANSWER_CONTENT = "answer_content";

    /**
     * The constant KYPO_ELASTICSEARCH_USER_REF_ID.
     */
    public static final String KYPO_ELASTICSEARCH_USER_REF_ID = "user_ref_id";

    /**
     * The constant KYPO_ELASTICSEARCH_USER_REF_ID.
     */
    public static final String KYPO_ELASTICSEARCH_ACCESS_TOKEN = "access_token";

    /**
     * The constant KYPO_ELASTICSEARCH_COMMAND.
     */
    public static final String KYPO_ELASTICSEARCH_COMMAND = "cmd";

}
