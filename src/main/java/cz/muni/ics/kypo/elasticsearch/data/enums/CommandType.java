package cz.muni.ics.kypo.elasticsearch.data.enums;

public enum CommandType {
    /**
     * Bash command type.
     */
    BASH,
    /**
     * Msfconsole command type.
     */
    MSF
}