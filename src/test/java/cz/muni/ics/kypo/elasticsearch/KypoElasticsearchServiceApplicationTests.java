package cz.muni.ics.kypo.elasticsearch;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = TestElasticsearchServiceConfiguration.class)
class KypoElasticsearchServiceApplicationTests {

    @Test
    void contextLoads() {
    }

}
